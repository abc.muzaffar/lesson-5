
// ------ task-1 ------

// console.log("Request data...");

// const promise = new Promise((resolve, reject) => {

//     setTimeout(() => {
//         console.log("Preparing data...");

//         const data = {
//             server: "aws",
//             port: 2000,
//             status: "pending"
//         }

//         setTimeout(() => {
//             data.status = "success"
//             if (data.status == "success") resolve(data)
//             else reject("Not found 404")
//         }, 2000);

//     }, 2000)
// })

// promise
//     .then((data) => console.log("Data resevied: ", data))
//     .catch((err) => console.log(err))



// ------ task-2 ------

// fetch("https://jsonplaceholder.typicode.com/users")
//     .then((response) => response.json())
//     .then((data) => {
//        data = data.filter((item => item.id > 5 ))
//        console.log(data);
//     })
//     .catch((err) => console.log(err))

// -----------------------------------------------------------------

// async function getUser() {
//     try {
//         const response = await fetch("https://jsonplaceholder.typicode.com/users");
//         if (!response.ok)
//             throw new Error(`Error! status: ${response.status}`);

//         const result = await response.json();
//         console.log(result);

//     } catch (err) {
//         console.log(err.message);
//     }
// }

// getUser()

